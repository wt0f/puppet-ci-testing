def generate_json(validity, extension: '.json')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
{
  "test": "good JSON",
  "foo": "bar"
}
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
{
  "test": "bad JSON"
  "foo": "bar"
}
EOF
    end
  end

  return filebase + extension
end


def generate_yaml(validity, extension: '.yaml')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
---
  test: "good YAML"
  foo: "bar"
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
===
  test: "bad YAML",
  foo: "bar"
EOF
    end
  end

  return filebase + extension
end


def generate_ruby(validity, extension: '.rb')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/usr/bin/env ruby
puts "valid ruby!"
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/usr/bin/env ruby
put "invalid ruby!'
EOF
    end
  end

  return filebase + extension
end


def generate_python(validity, extension: '.py')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/usr/bin/env python
print("valid python!")
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/usr/bin/env python
print("invalid python!')
EOF
    end
  end

  return filebase + extension
end


def generate_perl(validity, extension: '.pl')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/usr/bin/env perl
print "valid ";
print "perl!"
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/usr/bin/env perl
print "invalid "
print "perl!"
EOF
    end
  end

  return filebase + extension
end


def generate_bash(validity, extension: '.sh')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/bin/bash
echo "valid shell!"
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
#!/bin/bash
echo "invalid shell!'
EOF
    end
  end

  return filebase + extension
end


def generate_erb(validity, extension: '.erb')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
<% puts "valid erb!" %>
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
<% puts "invalid "  <%= erb!" %>
EOF
    end
  end

  return filebase + extension
end


def generate_puppet(validity, extension: '.pp')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
puppet { 'good': }
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
puppet { "bad" }
EOF
    end
  end

  return filebase + extension
end


def generate_gitlab_ci(validity, extension: '.gitlab-ci.yml')
  filebase = random_string(16)
  if validity == :valid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
stages:
  - stage1

job1:
  stage: stage1
  script:
    - echo "test"
EOF
    end
  elsif validity == :invalid
    open(filebase + extension, 'w') do |fh|
      fh.puts <<EOF
stages:
  stage1:

job1:
  stage: stage2
  script:
    echo "test"
    echo "test2"
EOF
    end
  end

  return filebase + extension
end
